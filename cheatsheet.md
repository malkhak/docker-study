
# Docker cheatsheet

## docker default location

```
cd /var/lib/docker
```

## download image
```
docker pull hello-world
docker pull mysql
```

## list docker images
```
docker images
```

## test hello-world
```
docker run --name hello-world hello-world
```


## create network
```
docker network create mynet
```

## remove network
```
docker network rm mynet
```

## create volume
```
docker volume create myvolume
```

## inspect volume
```
docker inspect myvolume
```

## remove volume
```
docker volume rm my-vol
```


## run a container (with specific network, mount volume, env variables)
```
docker run --detach --name mysql \
--network mynet --publish 3306:3306 \
--mount source=myvolume,target=/var/lib/mysql \
--env MYSQL_ROOT_PASSWORD=password \
mysql:latest ;
```


# list running container
```
docker ps --all -s
```

## return container id with specify name (eg. hello-world)
```
docker ps --all --format "table {{.Names}} {{.ID}}" | grep "hello-world" | cut -d' ' -f2
```

## stop image  (docker stop [Container ID])
```
docker stop d2857d496760
```

## remove image  (docker rm [Container ID])
```
docker rm d2857d496760
```

## get the container ip
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' 548f547a61cb
```

## enter docker shell
```
docker exec -it mysql bash
```

For exmaple,
```
mysql -u root -ppassword
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';

ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'new_user'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'new_user'@'%';
```

Connect from host (with the container ip)
```
mysql -h 172.18.0.2 -u root -ppassword
```

# Using docker-compose yml files

## Create and start containers
```
docker-compose up -d
```

## Stops containers and removes containers, networks, volumes, and images created by up
```
docker-compose stop
```












