

## clone git repo
git clone https://gitlab.com/malkhak/docker-study.git

## add macvlan mod (if needed)
sudo modprobe macvlan

## go to the guacamole docker compose directory
cd docker-study/guacamole

## remove existing mysql volume
sudo rm -rf mysql_data
docker compose up -d

## prepare the init sql script
docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --mysql > initdb.sql

## execute the sql script according to docker network type (change ip if needed)
mysql -h 172.24.1.34 -P 3306 -u root -proot111 guacamole < initdb.sql
mysql -h 192.168.1.33 -P 3306 -u root -proot111 guacamole < initdb.sql
mysql -h 127.0.0.1 -P 3306 -u root -proot111 guacamole < initdb.sql

## restart the guacamole container
docker restart guacamole

## Harden tomcat (optional), as-per https://www.owasp.org/index.php/Securing_tomcat

## open http://192.168.1.72:8080/guacamole/#/
guacadmin
guacadmin

## follow the tutorial to config remote config
https://www.youtube.com/watch?v=LWdxhZyHT_8&ab_channel=TechnoTim
image.png